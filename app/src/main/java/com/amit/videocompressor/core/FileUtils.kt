package com.amit.videocompressor.core

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import java.io.*
import java.net.URLDecoder

object FileUtils {
    fun getPath(context: Context, uri: Uri): String? {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) { // ExternalStorageProvider
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                val storageDefinition: String
                return if ("primary".equals(type, ignoreCase = true)) {
                    Environment.getExternalStorageDirectory().absolutePath + "/" + split[1]
                } else {
                    storageDefinition = if (Environment.isExternalStorageRemovable()) {
                        "EXTERNAL_STORAGE"
                    } else {
                        "SECONDARY_STORAGE"
                    }
                    val externalStorage = System.getenv(storageDefinition)
                    val splitpath = externalStorage.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val returnPath: String
                    returnPath = if (splitpath[2].equals(type, ignoreCase = true)) {
                        externalStorage + "/" + split[1]
                    } else {
                        "/" + splitpath[1] + "/" + split[0] + "/" + split[1]
                    }
                    returnPath
                }
            } else if (isDownloadsDocument(uri)) { // DownloadsProvider
                val id = DocumentsContract.getDocumentId(uri)
                var returnPath: String? = null
                if (id != null) {
                    if (id.startsWith("raw:")) {
                        returnPath = id.substring(4)
                    } else if (id.contains("/")) {
                        returnPath = if (id.startsWith("/")) {
                            id
                        } else {
                            id.substring(id.indexOf("/"))
                        }
                    } else {
                        val contentUriPrefixesToTry = arrayOf(
                                "content://downloads/public_downloads",
                                "content://downloads/my_downloads"
                        )
                        for (contentUriPrefix in contentUriPrefixesToTry) {
                            val contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), java.lang.Long.valueOf(id))
                            try {
                                val path = getDataColumn(context, contentUri, null, null)
                                if (path != null) {
                                    returnPath = path
                                    break
                                }
                            } catch (e: Exception) {
                            }
                        }
                    }
                }
                return returnPath
            } else if (isMediaDocument(uri)) { // MediaProvider
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                        split[1]
                )
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if (uri.toString().startsWith("content://com.lenovo.FileBrowser.FileProvider/root_path")) {
            try {
                return URLDecoder.decode(uri.toString().replace("content://com.lenovo.FileBrowser.FileProvider/root_path", ""), "UTF-8")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        } else return if ("content".equals(uri.scheme, ignoreCase = true)) { // MediaStore (and general)
            // Return the remote address
            if (isGooglePhotosUri(uri)) {
                uri.lastPathSegment
            } else getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.scheme, ignoreCase = true)) { // File
            uri.path
        } else {
            uri.toString()
        }
        return null
    }

    @Throws(IOException::class)
    fun copyAndRename(src: File?, dst: File?) {
        FileInputStream(src).use { `in` ->
            FileOutputStream(dst).use { out ->
                // Transfer bytes from in to out
                val buf = ByteArray(1024)
                var len: Int
                while (`in`.read(buf).also { len = it } > 0) {
                    out.write(buf, 0, len)
                }
            }
        }
    }

    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
                column
        )
        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val column_index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } catch (throwable: Throwable) {
            return null
        } finally {
            cursor?.close()
        }

        /*try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }*/return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    /*for check uri is from gmail attachment*/
    fun isGoogleAttachmentApi(uri: Uri): Boolean {
        return "com.google.android.gm.sapi" == uri.authority
    }

    /*for check uri is from google drive*/
    fun isGoogleDriveStorageApi(uri: Uri): Boolean {
        return "com.google.android.apps.docs.storage.legacy" == uri.authority
    }

    fun isGoogleUri(uri: Uri): Boolean {
        return uri.authority != null && uri.authority!!.startsWith("com.google.android")
    }

    fun getFileName(context: Context, fileUri: Uri?): String? {
        var fileName: String? = null
        try {
            val returnCursor = context.contentResolver.query(fileUri!!, null, null, null, null)
            if (returnCursor != null) {
                returnCursor.moveToFirst()
                val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                val sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE)
                fileName = returnCursor.getString(nameIndex)
                Log.d("Name", returnCursor.getString(nameIndex))
                Log.d("size", java.lang.Long.toString(returnCursor.getLong(sizeIndex)))
                returnCursor.close()
            }
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
        }
        return fileName
    }

    fun getExtension(path: String?): String? {
        if (path == null) {
            return null
        }
        val dot = path.lastIndexOf(".")
        return if (dot >= 0) {
            path.substring(dot)
        } else {
            // No extension.
            ""
        }
    }
}