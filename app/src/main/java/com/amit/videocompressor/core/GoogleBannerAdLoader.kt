package com.amit.videocompressor.core

import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import com.amit.videocompressor.BuildConfig
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView

class GoogleBannerAdLoader {
    private var adView: AdView? = null

    fun loadBannerAd(linearLayout: LinearLayoutCompat) {
        // Initialize the AdView
        adView = AdView(linearLayout.context)
        adView?.setAdSize(AdSize.BANNER)
        adView?.adUnitId =
            if (BuildConfig.DEBUG) Constant.GOOGLE_TEST_AD_ID else BuildConfig.BANNER_AD_UNIT_ID

        // Add the AdView to the LinearLayout
        val layoutParams = LinearLayoutCompat.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        adView?.layoutParams = layoutParams
        linearLayout.addView(adView)

        // Load the ad
        val adRequest = AdRequest.Builder().build()
        adView?.loadAd(adRequest)
    }

    fun onAdviewPause() {
        adView?.pause()
    }

    fun adViewResume() {
        adView?.resume()
    }

    fun destroyBannerAd() {
        adView?.destroy()
    }
}