package com.amit.videocompressor.core

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.amit.videocompressor.R

abstract class BaseActivity : AppCompatActivity(), View.OnClickListener {
    var mActivity: Activity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        changeStatusBarColor(R.color.colorPrimary)
    }

    override fun onClick(view: View) {}

    private fun changeStatusBarColor(color: Int) {
        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, color)
    }

    fun pushFragmentDontIgnoreCurrent(fragment: Fragment?, fragmentTransactionType: Int) {
        when (fragmentTransactionType) {
            Constant.FRAGMENT_JUST_REPLACE -> pushFragments(R.id.flContainer, null, fragment, false, false, false, false, false)
            Constant.FRAGMENT_JUST_ADD -> pushFragments(R.id.flContainer, null, fragment, false, false, false, true, false)
            Constant.FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE -> pushFragments(R.id.flContainer, null, fragment, false, true, false, false, false)
            Constant.FRAGMENT_ADD_TO_BACKSTACK_AND_ADD -> pushFragments(R.id.flContainer, null, fragment, false, true, false, true, false)
        }
    }

    fun pushFragmentIgnoreCurrent(fragment: Fragment?, fragmentTransactionType: Int) {
        when (fragmentTransactionType) {
            Constant.FRAGMENT_JUST_REPLACE -> pushFragments(R.id.flContainer, null, fragment, false, false, true, false, false)
            Constant.FRAGMENT_JUST_ADD -> pushFragments(R.id.flContainer, null, fragment, true, false, true, true, false)
            Constant.FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE -> pushFragments(R.id.flContainer, null, fragment, true, true, true, false, false)
            Constant.FRAGMENT_ADD_TO_BACKSTACK_AND_ADD -> pushFragments(R.id.flContainer, null, fragment, true, true, true, true, false)
        }
    }

    fun pushFragments(id: Int, args: Bundle?,
                      fragment: Fragment?, shouldAnimateLeftRight: Boolean,
                      shouldAdd: Boolean, ignoreIfCurrent: Boolean, justAdd: Boolean, shouldAnimateTopBottom: Boolean) {
        try {
            hideKeyboard()
            val currentFragment = supportFragmentManager.findFragmentById(R.id.flContainer)
            if (ignoreIfCurrent && currentFragment != null && fragment != null && currentFragment.javaClass == fragment.javaClass) {
                return
            }

            // assert fragment != null;
            if (fragment!!.arguments == null) {
                fragment.arguments = args
            }
            val fragmentManager = supportFragmentManager
            val ft = fragmentManager.beginTransaction()
            if (shouldAdd) {
                ft.addToBackStack(null)
            }
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            if (justAdd) {
                //System.out.println("if");
                removeIfExists(fragmentManager, fragment)
                ft.add(id, fragment, fragment.javaClass.canonicalName)
                if (supportFragmentManager.findFragmentById(R.id.flContainer) != null) {
                    // Fragment fragmentNew = context.getSupportFragmentManager().findFragmentById(R.id.container);
                    ft.hide(supportFragmentManager.findFragmentById(R.id.flContainer)!!)
                }
            } else {
                ft.replace(R.id.flContainer, fragment)
            }
            ft.commitAllowingStateLoss()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun hideKeyboard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
        }
    }

    private fun removeIfExists(fragmentManager: FragmentManager, fragment: Fragment?) {
        val f = fragmentManager.findFragmentByTag(fragment!!.javaClass.canonicalName)
        if (f != null && f.javaClass == fragment.javaClass) {
            fragmentManager.beginTransaction().remove(f).commit()
        }
    }

    fun clearBackStackFragmets() {
        // in my case I get the support fragment manager, it should work with the native one too
        val fragmentManager = supportFragmentManager
        // this will clear the back stack and displays no animation on the screen
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun clearBackStack() {
        val fm = supportFragmentManager
        val count = fm.backStackEntryCount
        if (count > 0) {
            for (i in 0 until count) {
                fm.popBackStack()
            }
        }
    }
}