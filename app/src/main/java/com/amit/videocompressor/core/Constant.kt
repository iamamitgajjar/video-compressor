package com.amit.videocompressor.core

import android.content.SharedPreferences
import java.text.SimpleDateFormat
import java.util.Locale

class Constant {
    companion object {
        val serverDateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
        val durationFormat = SimpleDateFormat("MMM dd, yyyy", Locale.US)
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)

        @JvmField
        var m_sharedPreference: SharedPreferences? = null

        @JvmField
        var m_sharedPrefEditor: SharedPreferences.Editor? = null

        @JvmField
        var IS_DEBUG = true
        const val REQUEST_CODE_GPS_SETTING = 100
        const val REQUEST_CODE_INTERNET_SETTING = 200
        var LOW_LEVEL = 1
        var MEDIUM_LEVEL = 2
        var HIGH_LEVEL = 3
        const val FUNDRAISER = "fundraiser"
        const val FRAGMENT_JUST_REPLACE = 0
        const val FRAGMENT_JUST_ADD = 1
        const val FRAGMENT_ADD_TO_BACKSTACK_AND_REPLACE = 2
        const val FRAGMENT_ADD_TO_BACKSTACK_AND_ADD = 3
        const val MAX_ALBUM_ART_SIZE = 1024
        const val GOOGLE_TEST_AD_ID = "ca-app-pub-3940256099942544/6300978111"
        const val GOOGLE_APP_OPEN_TEST_AD_ID = "ca-app-pub-3940256099942544/3419835294"
    }
}