package com.amit.videocompressor.core

import android.graphics.Bitmap

/**
 * Created by NEERAJ on 7/16/2017.
 */
interface ImageCallback {
    fun onDownload(bitmap: Bitmap?)
}