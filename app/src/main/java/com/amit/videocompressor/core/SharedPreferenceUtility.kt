package com.amit.videocompressor.core

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object SharedPreferenceUtility {
    /*public static void saveUser(Context context, User user) {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString(Const.USER_INFO, json);
        prefsEditor.commit();
    }*/
    /*public static User getUser(Context context) {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = mPrefs.getString("USER_INFO", "");
        User user = gson.fromJson(json, User.class);
        return user;
    }*/
    fun saveBoolean(context: Context?, tag: String?, data: Boolean) {
        val mPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val prefsEditor = mPrefs.edit()
        prefsEditor.putBoolean(tag, data)
        prefsEditor.commit()
    }

    fun getBoolean(context: Context?, tag: String?): Boolean {
        val mPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        return mPrefs.getBoolean(tag, false)
    }

    fun saveFundraiserSequence(context: Context?, list: ArrayList<String?>?) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        val gson = Gson()
        val s = gson.toJson(list)
        editor.putString(Constant.FUNDRAISER, s)
        editor.commit()
    }

    fun getFundraiserSequence(context: Context?): ArrayList<String?> {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val s = preferences.getString(Constant.FUNDRAISER, "")
        val type = object : TypeToken<ArrayList<String?>?>() {}.type
        var list = gson.fromJson<ArrayList<String?>>(s, type)
        if (list == null) list = ArrayList()
        return list
    }
}