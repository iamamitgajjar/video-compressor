package com.amit.videocompressor.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.amit.videocompressor.activity.HomeActivity
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.core.BaseApplication
import com.amit.videocompressor.databinding.ActivitySplashBinding

private const val COUNTER_TIME = 5L
private const val LOG_TAG = "SplashActivity"

class SplashActivity : BaseActivity() {
    lateinit var binding: ActivitySplashBinding
    private var secondsRemaining: Long = 0L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.lottieAnim.playAnimation()
        Handler(Looper.getMainLooper()).postDelayed({ binding.lottieAnim.pauseAnimation() }, 1500)
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(mActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }, 3000)

//        createTimer(COUNTER_TIME)
    }

    /**
     * Create the countdown timer, which counts down to zero and show the app open ad.
     *
     * @param seconds the number of seconds that the timer counts down from
     */
    private fun createTimer(seconds: Long) {
        val countDownTimer: CountDownTimer = object : CountDownTimer(seconds * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000 + 1
                binding.timer.text = "App is done loading in: $secondsRemaining"
            }

            override fun onFinish() {
                secondsRemaining = 0
                binding.timer.text = "Done."

                val application = application as? BaseApplication

                // If the application is not an instance of MyApplication, log an error message and
                // start the MainActivity without showing the app open ad.
                if (application == null) {
                    Log.e(LOG_TAG, "Failed to cast application to MyApplication.")
                    startMainActivity()
                    return
                }

                // Show the app open ad.
                application.showAdIfAvailable(
                    this@SplashActivity,
                    object : BaseApplication.OnShowAdCompleteListener {
                        override fun onShowAdComplete() {
                            startMainActivity()
                        }
                    })
            }
        }
        countDownTimer.start()
    }

    /** Start the MainActivity. */
    fun startMainActivity() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        finish()
    }
}