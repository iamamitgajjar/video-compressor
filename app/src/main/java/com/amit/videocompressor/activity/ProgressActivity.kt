package com.amit.videocompressor.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.amit.videocompressor.R
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.core.CommonUtils
import com.amit.videocompressor.core.Constant
import com.amit.videocompressor.databinding.ActivityProgressBinding
import com.vincent.videocompressor.VideoCompress
import com.vincent.videocompressor.VideoCompress.CompressListener
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import me.itangqi.waveloadingview.WaveLoadingView
import java.io.File
import kotlin.math.roundToInt

class ProgressActivity : BaseActivity() {
    lateinit var binding: ActivityProgressBinding
    private var compressListener: CompressListener? = null
    private val disposable = CompositeDisposable()
    private var dialog: SweetAlertDialog? = null
    private var compressionType = 0
    private var fileUri: String? = null
    private var savePath: String? = null
    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivityProgressBinding.inflate(layoutInflater)
        setContentView(binding.root)
        dialog = SweetAlertDialog(mActivity)
        dialog!!.setCanceledOnTouchOutside(false)
        binding.waveLoadingView.setShapeType(WaveLoadingView.ShapeType.RECTANGLE)
        binding.waveLoadingView.progressValue = 0
        binding.waveLoadingView.setAmplitudeRatio(10)
        binding.waveLoadingView.waveColor = ContextCompat.getColor(mActivity!!, R.color.white_trans)
        fileUri = intent.getStringExtra("fileUri")
        compressionType = intent.getIntExtra("compressionType", 0)
        binding.tvProgress.text = "0%"
        compressListener = object : CompressListener {
            override fun onStart() {
                CommonUtils.makeToast(mActivity, "Compressing Video...")
            }

            override fun onSuccess() {
                Log.e(TAG, "onSuccess: ")
                binding.waveLoadingView.pauseAnimation()
                binding.tvProgress.text = "100%"
                dialog!!.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                dialog!!.showContentText(true)
                dialog!!.contentText = "Save Path:\n$savePath"
                dialog!!.titleText = "Success"
                dialog!!.setCancelButton("Close") {
                    dialog!!.dismissWithAnimation()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                dialog!!.setConfirmButton("Open") {
                    dialog!!.dismissWithAnimation()
                    if (!CommonUtils.isEmpty(savePath)) {
                        val intent = Intent(mActivity, PlayerActivity::class.java)
                        intent.putExtra("url", savePath)
                        startActivityForResult(intent, 5001)
                    }
                }
                dialog!!.show()
            }

            override fun onFail() {
                Log.e(TAG, "onFail: ")
                binding.waveLoadingView.pauseAnimation()
                dialog!!.changeAlertType(SweetAlertDialog.ERROR_TYPE)
                dialog!!.titleText = "Oops!"
                dialog!!.contentText = "Something went Wrong"
                dialog!!.setConfirmButton("Okay") { sweetAlertDialog: SweetAlertDialog? ->
                    dialog!!.dismissWithAnimation()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                dialog!!.show()
            }

            override fun onProgress(percent: Float) {
//                binding.waveView.setProgress(Math.round(percent));
                binding.waveLoadingView.progressValue = percent.roundToInt()
                binding.tvProgress.text = "${percent.roundToInt()}%"
            }
        }
        setupClicks()
        if (!CommonUtils.isEmpty(fileUri) && compressionType != 0) {
            compress()
        }
    }

    private fun setupClicks() {}
    override fun onBackPressed() {
        if (count == 0) {
            CommonUtils.makeToast(mActivity, "Press back again to Cancel Compressing")
            count++
        } else if (count > 0) {
            super.onBackPressed()
        }
        Handler(Looper.getMainLooper()).postDelayed({ count = 0 }, 2000)
    }

    private fun compressNow() {
        disposable.add(observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dialog!!.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
                dialog!!.showContentText(false)
                dialog!!.titleText = "Success"
                dialog!!.setConfirmButton("Done") {
                    dialog!!.dismissWithAnimation()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }) {
                dialog!!.changeAlertType(SweetAlertDialog.ERROR_TYPE)
                dialog!!.titleText = "Oops!"
                dialog!!.contentText = "Something went Wrong"
                dialog!!.setConfirmButton("Okay") {
                    dialog!!.dismissWithAnimation()
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            })
    }

    private val observable: Observable<String>
        get() = Observable.fromCallable {
            val saveFilePath = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MOVIES), "VideoCompressor" + System.currentTimeMillis() + "Type" + compressionType + ".mp4")
            if (saveFilePath.mkdirs() || saveFilePath.isDirectory) {
                when (compressionType) {
                    Constant.LOW_LEVEL -> {
                        mediaCompressor(fileUri, saveFilePath.path, Constant.LOW_LEVEL)
                    }

                    Constant.MEDIUM_LEVEL -> {
                        mediaCompressor(fileUri, saveFilePath.path, Constant.MEDIUM_LEVEL)
                    }

                    Constant.HIGH_LEVEL -> {
                        mediaCompressor(fileUri, saveFilePath.path, Constant.HIGH_LEVEL)
                    }
                }
            }
            ""
        }

    private fun compress() {
        val saveFilePath = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ), "VideoCompressor" + System.currentTimeMillis() + "Type" + compressionType + ".mp4"
        )
        savePath = saveFilePath.path
        when (compressionType) {
            Constant.LOW_LEVEL -> {
                mediaCompressor(fileUri, saveFilePath.path, Constant.LOW_LEVEL)
            }

            Constant.MEDIUM_LEVEL -> {
                mediaCompressor(fileUri, saveFilePath.path, Constant.MEDIUM_LEVEL)
            }

            Constant.HIGH_LEVEL -> {
                mediaCompressor(fileUri, saveFilePath.path, Constant.HIGH_LEVEL)
            }
        }
    }

    @Suppress("INACCESSIBLE_TYPE")
    private fun mediaCompressor(src: String?, dest: String, compressionType: Int) {
        when (compressionType) {
            3 -> VideoCompress.compressVideoLow(src, dest, compressListener)
            2 -> VideoCompress.compressVideoMedium(src, dest, compressListener)
            else -> VideoCompress.compressVideoHigh(src, dest, compressListener) // else 1
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 5001) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    companion object {
        private val TAG = ProgressActivity::class.java.simpleName
    }
}