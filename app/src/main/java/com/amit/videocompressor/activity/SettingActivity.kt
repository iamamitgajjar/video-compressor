package com.amit.videocompressor.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.CompoundButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.amit.videocompressor.BuildConfig
import com.amit.videocompressor.R
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.core.CommonUtils
import com.amit.videocompressor.databinding.ActivitySettingBinding

class SettingActivity : BaseActivity() {
    private val PERMISSION_REQUEST_CODE = 2
    lateinit var binding: ActivitySettingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.tvVersion.text = "Version: ${BuildConfig.VERSION_NAME} (BETA)"
        binding.swHwAcceleration.setOnCheckedChangeListener { _: CompoundButton?, _: Boolean ->
            binding.swHwAcceleration.isChecked = true
            CommonUtils.makeToast(mActivity, "Enabled by Default")
        }
        if (checkPermission()) {
            val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath
            binding.tvSavePath.text = path
        } else {
            requestPermission()
        }
        setupClicks()
    }

    private fun setupClicks() {
        binding.swHwAcceleration.setOnClickListener(this)
        binding.tvPrivacyPolicy.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.swHwAcceleration -> {}
            R.id.tvPrivacyPolicy -> {
                startActivity(Intent(this, WeviewActivity::class.java))
            }
        }
    }

    /*permission for capture image or video in camera*/
    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(mActivity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mActivity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    /*permission for capture image or video in camera*/
    private fun requestPermission() {
        ActivityCompat.requestPermissions(mActivity!!, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath
                binding.tvSavePath.text = path
            }
        }
    }
}