package com.amit.videocompressor.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.format.Formatter
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.amit.videocompressor.R
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.core.CommonUtils
import com.amit.videocompressor.core.Constant
import com.amit.videocompressor.core.FileUtils
import com.amit.videocompressor.core.GoogleBannerAdLoader
import com.amit.videocompressor.databinding.ActivityHomeBinding
import com.bumptech.glide.Glide
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.io.File

class HomeActivity : BaseActivity() {
    private val PERMISSION_REQUEST_CODE = 2
    private lateinit var binding: ActivityHomeBinding
    private var type: String? = null
    private var fileName: String? = null
    private var fileUri: Uri? = null
    private val disposable: CompositeDisposable = CompositeDisposable()
    private var compression_type = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        GoogleBannerAdLoader().loadBannerAd(binding.adView)
        setupClicks()
        binding.tvLow.performClick()
    }

    private fun setupClicks() {
        binding.tvStatus.setOnClickListener(this)
        binding.btnCompress.setOnClickListener(this)
        binding.llChooseVideo.setOnClickListener(this)
        binding.llVideo.setOnClickListener(this)
        binding.tvLow.setOnClickListener(this)
        binding.tvMedium.setOnClickListener(this)
        binding.tvHigh.setOnClickListener(this)
        binding.tvRemove.setOnClickListener(this)
        binding.ivSetting.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvLow -> if (!binding.tvLow.isSelected) {
                binding.tvLow.isSelected = true
                binding.tvMedium.isSelected = false
                binding.tvHigh.isSelected = false
                binding.tvLow.setTextColor(resources.getColor(R.color.white))
                binding.tvMedium.setTextColor(resources.getColor(R.color.colorPrimary))
                binding.tvHigh.setTextColor(resources.getColor(R.color.colorPrimary))
                compression_type = Constant.LOW_LEVEL
            }
            R.id.tvMedium -> if (!binding.tvMedium.isSelected) {
                binding.tvMedium.isSelected = true
                binding.tvLow.isSelected = false
                binding.tvHigh.isSelected = false
                binding.tvLow.setTextColor(resources.getColor(R.color.colorPrimary))
                binding.tvMedium.setTextColor(resources.getColor(R.color.white))
                binding.tvHigh.setTextColor(resources.getColor(R.color.colorPrimary))
                compression_type = Constant.MEDIUM_LEVEL
            }
            R.id.tvHigh -> if (!binding.tvHigh.isSelected) {
                binding.tvHigh.isSelected = true
                binding.tvLow.isSelected = false
                binding.tvMedium.isSelected = false
                binding.tvLow.setTextColor(resources.getColor(R.color.colorPrimary))
                binding.tvMedium.setTextColor(resources.getColor(R.color.colorPrimary))
                binding.tvHigh.setTextColor(resources.getColor(R.color.white))
                compression_type = Constant.HIGH_LEVEL
            }
            R.id.llChooseVideo -> if (checkPermission()) {
                selectVideo()
            } else {
                requestPermission()
            }
            R.id.llVideo -> if (fileUri != null && !CommonUtils.isEmpty(fileUri!!.path)) {
                val intent = Intent(mActivity, PlayerActivity::class.java)
                intent.putExtra("url", fileUri!!.path)
                startActivity(intent)
            } else {
                CommonUtils.makeToast(mActivity, "Unable to Play video")
            }
            R.id.btnCompress -> if (fileUri != null && !CommonUtils.isEmpty(fileUri!!.path)) {
                val intent = Intent(mActivity, ProgressActivity::class.java)
                intent.putExtra("fileUri", fileUri!!.path)
                intent.putExtra("compressionType", compression_type)
                startActivityForResult(intent, 1001)
            } else {
                Toast.makeText(mActivity, "Please Select Video First", Toast.LENGTH_SHORT).show()
            }
            R.id.tvRemove -> {
                removeVideo()
                showChooseVideoLayout(true)
            }
            R.id.ivSetting -> {
                val intent = Intent(mActivity, SettingActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun showChooseVideoLayout(showChooseVideo: Boolean) {
        binding.llChooseVideo.visibility = if (showChooseVideo) View.VISIBLE else View.GONE
        binding.llVideo.visibility = if (showChooseVideo) View.GONE else View.VISIBLE
    }

    /*permission for capture image or video in camera*/
    private fun checkPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ContextCompat.checkSelfPermission(
                mActivity!!,
                Manifest.permission.READ_MEDIA_VIDEO
            ) == PackageManager.PERMISSION_GRANTED
        } else {
            ContextCompat.checkSelfPermission(
                mActivity!!,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(
                        mActivity!!,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
        }
    }

    /*permission for capture image or video in camera*/
    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.requestPermissions(
                mActivity!!,
                arrayOf(
                    Manifest.permission.READ_MEDIA_VIDEO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                PERMISSION_REQUEST_CODE
            )
        } else {
            ActivityCompat.requestPermissions(
                mActivity!!,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectVideo()
                } else Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
            } else {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    selectVideo()
                } else Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
            }

            else -> {
                Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    /*capturing video from camera*/
    private fun captureVideoFromCamera() {
        val saveFilePath = File(Environment.getExternalStorageDirectory().absolutePath
                + File.separator + mActivity?.packageName)
        if (!saveFilePath.exists()) {
            saveFilePath.mkdirs()
        }
        val fileName = "Roots_VID_" + System.currentTimeMillis() + ".mp4"
        fileUri = Uri.fromFile(File(saveFilePath.toString() + File.separator + fileName))
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120)
        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(takeVideoIntent, 104)
        Log.e("VideoUri", "outputFileUri intent$fileUri")
    }

    /*open video from gallery*/
    private fun openGalleryForVideo() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Video"), 105)
    }

    /*open dialog for selecting video*/
    fun selectVideo() {
        /*val options = arrayOf<CharSequence>("Camera", "Gallery", "Cancel")
        val builder = AlertDialog.Builder(mActivity)
        builder.setTitle("Select Video From")
        builder.setItems(options) { dialog, item ->
            if (options[item] == "Camera") {
                captureVideoFromCamera()
            } else if (options[item] == "Gallery") {
                openGalleryForVideo()
            } else if (options[item] == "Cancel") {
                dialog.dismiss()
            }
        }
        builder.show()*/
        openGalleryForVideo()
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var fileSize = ""
        if (requestCode == 104 && resultCode == Activity.RESULT_OK) {
            val path = FileUtils.getPath(mActivity!!, fileUri!!)

            if (CommonUtils.isEmpty(path)) {
                Toast.makeText(mActivity, "Can't Access Video", Toast.LENGTH_SHORT).show()
                return
            }
            fileUri = Uri.parse(path)
            type = "video"
            if (fileUri != null) {
                val file = File(fileUri.toString())
                if (file != null && file.isFile) {
                    fileSize = Formatter.formatShortFileSize(mActivity, file.length())
                    Log.e(TAG, "onActivityResult: Size: $fileSize")
                }
            }
            showChooseVideoLayout(false)
            fileName = CommonUtils.getFileName(fileUri)
            if (!CommonUtils.isEmpty(fileName)) {
                binding.tvFileName.text = "$fileSize $fileName".trimIndent()
            } else {
                binding.tvFileName.text = "Unknown.mp4"
            }
            try {
                val bitmap = Glide.with(mActivity!!)
                        .asBitmap()
                        .submit(500, 500).get()
//                val bitmap = ThumbnailUtils.createVideoThumbnail(fileUri?.getPath()!!, MediaStore.Images.Thumbnails.MINI_KIND)
                binding.ivThumb.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == 105 && resultCode == Activity.RESULT_OK) {
            /*choose video from gallery*/
            val path = FileUtils.getPath(mActivity!!, data!!.data!!)
            if (CommonUtils.isEmpty(path)) {
                Toast.makeText(mActivity, "Can't Access Video", Toast.LENGTH_SHORT).show()
                return
            }
            fileUri = Uri.parse(path)
            type = "video"
            if (fileUri != null) {
                val file = File(fileUri.toString())
                if (file != null && file.isFile) {
                    fileSize = Formatter.formatShortFileSize(mActivity, file.length())
                    Log.e(TAG, "onActivityResult: Size: $fileSize")
                }
            }
            binding.llChooseVideo.visibility = View.GONE
            binding.llVideo.visibility = View.VISIBLE
            fileName = CommonUtils.getFileName(fileUri)
            if (!CommonUtils.isEmpty(fileName)) {
                binding.tvFileName.text = """
                    $fileName
                    $fileSize
                    """.trimIndent()
            } else {
                binding.tvFileName.text = "Unknown.mp4"
            }
            try {
                val bitmap = ThumbnailUtils.createVideoThumbnail(fileUri?.path!!, MediaStore.Images.Thumbnails.MINI_KIND)
                binding.ivThumb.setImageBitmap(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (requestCode == 1001 && resultCode == Activity.RESULT_OK) {
            removeVideo()
            showChooseVideoLayout(true)
            CommonUtils.makeToast(mActivity, "Video Saved!")
        }
    }

    private fun removeVideo() {
        fileName = null
        fileUri = null
    }

    override fun onDestroy() {
        if (disposable != null && !disposable.isDisposed) {
            disposable.dispose()
        }
        super.onDestroy()
        GoogleBannerAdLoader().destroyBannerAd()
    }

    fun addImageToGallery(filePath: String?) {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
        values.put(MediaStore.Images.Media.MIME_TYPE, "video/mp4")
        values.put(MediaStore.MediaColumns.DATA, filePath)
        contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    }

    companion object {
        private val TAG = HomeActivity::class.java.simpleName
    }

    /** Override the default implementation when the user presses the back key. */
    override fun onBackPressed() {
        // Move the task containing the MainActivity to the back of the activity stack, instead of
        // destroying it. Therefore, MainActivity will be shown when the user switches back to the app.
        moveTaskToBack(true)
    }
}