package com.amit.videocompressor.activity

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.databinding.ActivityPlayerBinding

class PlayerActivity : BaseActivity() {
    lateinit var binding: ActivityPlayerBinding
    private var url: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        url = intent.getStringExtra("url")
        if (url != null && !url.isNullOrEmpty()) {
            binding.videoView.setOnPreparedListener { binding.videoView.start() }
            binding.videoView.setVideoURI(Uri.parse(url))
        }
        binding.videoView.setOnCompletionListener { finish() }
        binding.ivClose.setOnClickListener { v: View? -> finish() }
    }
}