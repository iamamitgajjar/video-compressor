package com.amit.videocompressor.activity

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.amit.videocompressor.BuildConfig
import com.amit.videocompressor.R
import com.amit.videocompressor.core.BaseActivity
import com.amit.videocompressor.core.CommonUtils
import com.amit.videocompressor.databinding.ActivityWeviewBinding


class WeviewActivity : BaseActivity() {
    private lateinit var binding: ActivityWeviewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this
        binding = ActivityWeviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupClicks()
        initWebView()
    }

    private fun setupClicks() {
        binding.ivClose.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        super.onClick(view)
        when (view.id) {
            R.id.ivClose -> finish()
        }
    }

    private fun initWebView() {
        binding.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                try {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(request?.url.toString())
                    startActivity(intent)
                } catch (e: Exception) {
                    CommonUtils.makeToast(this@WeviewActivity, "No external browser installed")
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                binding.progressWheel.isVisible = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                binding.progressWheel.isVisible = false
            }
        }
        binding.webView.loadUrl(BuildConfig.PRIVACY_POLICY_URL)
    }
}